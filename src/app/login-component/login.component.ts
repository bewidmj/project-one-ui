import { Component, Optional } from '@angular/core';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { User } from '../models/user';
import { Http, Response, RequestOptions, Headers} from '@angular/http'
import { Router } from '@angular/router';
import { } from '@angular/forms';
import 'rxjs/add/operator/map';
@Component({
  selector: 'login-component',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  userName: string;
  passWord: string;
  user: User;
  loginUrl = "http://localhost:8080/user/login";
  constructor(private http: Http, private router: Router){}
  login()
  {
    if(this.userName && this.passWord)
    {
      const formdata = new FormData();
      formdata.append("userName", this.userName);
      formdata.append("passWord", this.passWord)
      let headers = new Headers();
      headers.append('Accept', 'application/json');
      let options = new RequestOptions({ headers: headers });
      return this.http.post(this.loginUrl, formdata, options)
      .map((res:Response) => res.json())
      .subscribe(
        data => {
            this.handleResponse(data);
        },
          error => {
            this.handleError(JSON.parse(error.text()));
          }
        );
    }
  }
  handleError(error: any){
    console.log(error);
  }
  handleResponse(data: any){
    console.log(data);
    if(data.businessRole == "Admin"){
      this.router.navigate(['/dashboard']);
    }
    else{
      this.router.navigate(['/home']);
    }
  }
}