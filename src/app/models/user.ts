export class User {
    userName: String;
    firstName: String;
    passWord: String;
    lastName: String;
    emailId: String;
    businessRole: String;
	mobileNo: String;
}