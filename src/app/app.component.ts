import { Component, Optional, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { AppRoutingModule } from '././app.routing';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
constructor( private router: Router)
  {

  }
ngOnInit()
  {
  this.router.navigate(['/login']);
  }

}

