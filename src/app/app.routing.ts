import { ModuleWithProviders, NgModule }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home-component/home.component';
import { LoginComponent } from './login-component/login.component';
import { RegisterComponent } from './register-component/register.component';
import { DashboardComponent } from './dashboard-component/dashboard.component';

// Route Configuration
export const approutes: Routes = [
  { path: '', component: AppComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'home', component: HomeComponent },
  { path: 'dashboard', component: DashboardComponent }
];


@NgModule({
  imports: [
    RouterModule.forRoot(approutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
