import { Component, Optional } from '@angular/core';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { User } from '../models/user';
import { Http, Response, RequestOptions, Headers} from '@angular/http'
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
@Component({
  selector: 'register-component',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  userName: string;
  passWord: string;
  firstName: string;
  lastName: string;
  emailId: string;
  businessRole: String;
  mobileNo: String;
  
  registerUrl = "http://localhost:8080/user/register";
  constructor(private http: Http, private router: Router){}
  register()
  {
    if(this.userName && this.passWord)
    {
      let user = {passWord: this.passWord, userName: this.userName, firstName: this.firstName, lastName: this.lastName, emailId: this.emailId, businessRole :this.businessRole, mobileNo: this.mobileNo}
      let headers = new Headers({'Content-Type': 'application/json'});
      let options = new RequestOptions({ headers: headers });
      return this.http.post(this.registerUrl, JSON.stringify(user), options)
      .map((res:Response) => res.json())
      .subscribe(
        data => {
            this.handleResponse(data);
        },
          error => {
            this.handleError(JSON.parse(error.text()));
          }
        );
    }
  }
  handleError(error: any){
    console.log(error);
  }
  handleResponse(data: any){
    console.log(data);
    this.router.navigate(['/login']);
  }
}